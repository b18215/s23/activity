// S23 - Activity - Pokemon Game

function Pokemon(name, level, health){

	// Properties
	this.name = name;
	this.level = level;
	this.health = health * level;
	this.atk = level * 2;

	


	// Methods
	this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name} [DMG: ${this.atk}]`);

        let damageResult = target.health - this.atk;

        target.health = damageResult;

        console.log(`${target.name} health is now reduced to ${target.health}`);

        if(target.health < 10){
            target.faint();
            this.levelUp();

        }
    };


	this.faint = function(){
		console.warn(`${this.name} fainted`);	
	};

	this.levelUp = function(){
		console.warn(`${this.name} Level-up! (Level ${this.level + 1})`);	

		let LevelUp1 = this.level + 1;

        this.level = LevelUp1;
	};
};

let jigglypuff = new Pokemon("Jigglypuff", 5, 10);
let snorlax = new Pokemon("Snorlax", 5, 10);

console.log(jigglypuff);
console.log(snorlax);

jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);